<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="{{route('book.update', $book->id)}}" method="post">
    @csrf
    @method('put')
        <div>
            <label for="name">name</label>
            <input type="text" name="name" id="name" value="{{$book->name}}">
        </div>
        <div>
            <label for="author">author</label>
            <input type="text" name="author" id="author" value="{{$book->author}}">
        </div>
        <div>
            <label for="description">description</label>
            <input type="text" name="description" id="description" value="{{$book->description}}">
        </div>
        <div>
            <label for="releaseDate">releaseDate</label>
            <input type="date" name="releaseDate" id="releaseDate" value="{{$book->releaseDate}}">
        </div>
        <button>save</button>
    </form>
</body>
</html>
<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookRosources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //dd($this->resource->name);
        return [
            'type'=>'book',
            'id'=>$this->resource->getRouteKey(),
            'atributes'=>[
                'name'=>$this->resource->name, 
                'author'=>$this->resource->author, 
                'description'=>$this->resource->description,
                'releaseDate'=>$this->resource->releaseDate
            ],
            'links'=>route('api.book.show',$this->resource->getRouteKey())
        ];
    }
}

<?php

use Illuminate\Support\Facades\Route;


Route::get('/', 'BookController@index')->name('book.index');
Route::get('/create', 'BookController@create')->name('book.create');
Route::post('/store', 'BookController@store')->name('book.store');
Route::delete('/delete/{book}', 'BookController@destroy')->name('book.destroy');
Route::put('/edit/{book}', 'BookController@update')->name('book.update');
Route::get('/edit/{book}', 'BookController@edit')->name('book.edit');
Route::get('/show/{book}', 'BookController@show')->name('book.show');



<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::get('/','ApiBookController@index')->name('api.book.index');
Route::post('/store','ApiBookController@store')->name('api.book.store');
Route::post('/update/{book}','ApiBookController@update')->name('api.book.update');
Route::post('/delete/{book}','ApiBookController@delete')->name('api.book.delete');
Route::get('/show/{book}','ApiBookController@show')->name('api.book.show');

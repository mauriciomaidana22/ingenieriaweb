<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>

    <header class="mt-5" style="text-align: center;">
        <h1>PERSONAL</h1>
    </header>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-8 m-auto">
                    <a class="btn btn-success mb-5" href="{{route('Productos.create')}}">Nuevo</a>
                    <table class="table">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">N°</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Celular</th>
                            <th scope="col">Correo</th>
                            <th scope="col">Edad</th>
                            <th scope="col">Opciones</th>
                          </tr>
                        </thead>
                        <tbody>
                            
                              @foreach($productos as $i=> $productos)
                              
                              <tr>
                                <th scope="row">{{$i+1}}</th>
                                <td>{{$productos->Nombres}}</td>
                                <td>{{$productos->NombreApellidos}}</td>
                                <td>{{$productos->Celular}}</td>
                                <td>{{$productos->Correo}}</td>
                                <td>{{$productos->Edad}}</td>
                                <td><a class="btn btn-primary" href="{{route('Productos.edit',$productos->id)}}">Editar</a>
                                <form action="{{route('Productos.destroy',$productos->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger">Elliminar</button>
                                </form>
                              </tr>
                              
                              @endforeach
                              
                            
                          
                        </tbody>
                      </table>
                      
                      
                </div>
            </div>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

    
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

</head>
<body>
<header class="mt-5" style="text-align: center;">
        <h1>PERSONAL</h1>
    </header>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-8 m-auto">
                  <form method="POST" action="{{route('Productos.update',$producto->id)}}">
                    @csrf
                    @method('PUT')

                    <div class="form-group">
                      <label for="exampleInputPassword1">Nombre</label>
                      <input type="text" name="Nombres" class="form-control" id="nombre" value="{{$producto->Nombres}}">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Apellidos</label>
                      <input type="text" name="NombreApellidos" class="form-control" id="apellido" value="{{$producto->NombreApellidos}}">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Celular</label>
                      <input type="text" name="Celular" class="form-control" id="celular" value="{{$producto->Celular}}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="Correo" class="form-control" id="exampleInputEmail1" value="{{$producto->Correo}}" aria-describedby="emailHelp">
                      </div>
                      
                    <div class="form-group">
                      <label for="exampleInputPassword1">Edad</label>
                      <input type="text" name="Edad" class="form-control" id="edad" value="{{$producto->Edad}}">
                    </div>
                    
                    

                    <div class="form-group form-check">
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                  </form>
                      
                      
                </div>
            </div>
        </div>
    </section>
  



<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

    
</body>
</html>
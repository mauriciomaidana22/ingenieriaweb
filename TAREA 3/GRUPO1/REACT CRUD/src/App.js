import React, { useCallback, useEffect, useState } from 'react';
import UserTable from './components/UserTable';
import {v4 as uuidv4} from 'uuid';
import AddUserForm from './components/AddUserform';
import EditUserForm from './components/EditUserForm';
import axios from 'axios';
import Axios from 'axios';
const endpoint = "http://localhost:8000/api/person";
const App = () => {

  const [usersAPI, setUsersAPI] = useState([]);

  const loadUsers = useCallback(() => {
    axios.get(`${endpoint}`).then((response) => {
      setUsersAPI(response.data.data);
      console.log(response);
    }).catch(
      err => {
        console.log(err);
      }
    )
  }, []);

  useEffect(() => {
    loadUsers();
  }, [loadUsers]);

  const usersData = [
    { id: uuidv4(), first_name: 'alvaro',last_name: 'galarza' ,username: 'alvarinm', password: 'alvaro', gender: 'M' },
    { id: uuidv4(), first_name: 'martin',last_name: 'antezana' ,username: 'tincho', password: 'martin', gender: 'M' },
    { id: uuidv4(), first_name: 'ema',last_name: 'watson' ,username: 'emita', password: 'ema', gender: 'F' },
  ]
  const [users, setUsers] = useState(usersData); 

const addUser = (user) =>{
  user.id = uuidv4()
  setUsers([
    ... users,
    user  
  ])
}

const deleteUser =(id) => {

  Axios.delete("http://localhost:8000/api/person/"+id).then(response => {
    console.log(response);
}).catch(error1 => {
    console.log(error1);
});
  setUsers(users.filter(user => user.id !== id) )
 

}


const [editing, setEditing] = useState(false);

const [currentUser, setCurrentUser] = useState({
  id: null, first_name: '', last_name: '', username: '', gender: '' 
})

const editRow = (user) => {
  setEditing(true);
  setCurrentUser({
    id: user.id, first_name: user.first_name, username: user.username, last_name: user.last_name, gender: user.gender, password: user.password
    })
}

const updateUser = (id, updateUser) => {
  setEditing(false);
  setUsers(users.map(user => (user.id === id ? updateUser : user)))
}


  return (
    <div className="container">
      <h1>CRUD DE PRUEBA</h1>
      <div className="flex-row">
        <div className="flex-large">

{
  editing ?(
    <div>

        <h2>Editar usuario</h2>
        <EditUserForm
        currentUser = {currentUser}
        updateUser = {updateUser}/>
        

    </div>
  ):
  (
    <div>   
         <h2>Agregar usuario</h2>
          <AddUserForm addUser = {addUser}/> 
    </div>
  )
}
     
        </div>
        <div className="flex-large">
          <h2>Ver usuario</h2>
          <UserTable users = {usersAPI} deleteUser={deleteUser} editRow = {editRow}/>
        </div>
      </div>
    </div>
  )
}

export default App
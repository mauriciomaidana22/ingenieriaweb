import React from 'react';
import { useForm } from 'react-hook-form'
import axios from 'axios';

const AddUserForm = (props) => {

    const { register, errors, handleSubmit } = useForm();
    const onSubmit = (data, e) => {
        console.log(data)
        axios.post("http://localhost:8000/api/person", data).then(response => {
            console.log(response)
        }).catch(erro => {
            console.log(erro)
        });
        props.addUser(data)
        e.target.reset();
    }
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <label>Nombre</label>
            <input type="text" name="first_name" ref={
                register({
                    required: { value: true, message: 'campo requerido' }
                })
            } />
            <div>
                {errors?.first_name?.message}
            </div>
            <label>Apellido</label>
            <input type="text" name='last_name' ref={
                register({
                    required: { value: true, message: 'campo requerido' }
                })
            }></input>
            <div>
                {errors?.first_name?.message}
            </div>
            <label>Usuario</label>
            <input type="text" name="username" ref={
                register({
                    required: { value: true, message: 'campo requerido' }
                })
            } />
            <div>
                {errors?.username?.message}
            </div>
            <label>Password</label>
            <input type="text" name="password" ref={
                register({
                    required: { value: true, message: 'campo requerido' }
                })
            } />
            <div>
                {errors?.password?.message}
            </div>
            <label>Genero</label>
            <input type="text" name="gender" ref={
                register({
                    required: { value: true, message: 'campo requerido' }
                })
            } />
            <div>
                {errors?.gender?.message}
            </div>
            <button >Agregar usuario</button>
        </form>);
}

export default AddUserForm;
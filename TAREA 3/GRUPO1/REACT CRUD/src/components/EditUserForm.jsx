import Axios from 'axios';
import React from 'react';
import { useForm } from 'react-hook-form'
import loadUsers  from '../App';
const EditUserForm = (props) => {

    const { register, errors, handleSubmit, setValue } = useForm({
        defaultValues: props.currentUser
    });

    setValue('first_name', props.currentUser.first_name);
    setValue('username', props.currentUser.username);
    setValue('last_name', props.currentUser.last_name);
    setValue('password', props.currentUser.password);
    setValue('gender', props.currentUser.gender);
    setValue('id', props.currentUser.id);


    const onSubmit = (data, e) => {
        console.log(data);
        Axios.put("http://localhost:8000/api/person", data).then(response => {
            console.log(response);
        }).catch(error1 => {
            console.log(error1);
        });
        data.id = props.currentUser.id
        props.updateUser(props.currentUser.id, data)
        e.target.reset();
    }
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <label>ID</label>
            <input type="text" name="id" ref={
                register({
                    required: { value: true, message: 'campo requerido' }
                })
            } />
            <div>
                {errors?.first_name?.message}
            </div>
            <label>Nombre</label>
            <input type="text" name="first_name" ref={
                register({
                    required: { value: true, message: 'campo requerido' }
                })
            } />
            <div>
                {errors?.first_name?.message}
            </div>
            <label>Apellido</label>
            <input type="text" name="last_name" ref={
                register({
                    required: { value: true, message: 'campo requerido' }
                })
            } />
            <div>
                {errors?.last_name?.message}
            </div>
            <label>Usuario</label>
            <input type="text" name="username" ref={
                register({
                    required: { value: true, message: 'campo requerido' }
                })
            } />
            <div>
                {errors?.username?.message}
            </div>
            <label>Password</label>
            <input type="text" name="password" ref={
                register({
                    required: { value: true, message: 'campo requerido' }
                })
            } />
            <div>
                {errors?.password?.message}
            </div> <label>Genero</label>
            <input type="text" name="gender" ref={
                register({
                    required: { value: true, message: 'campo requerido' }
                })
            } />
            <div>
                {errors?.gender?.message}
            </div>
            <button >Editar usuario</button>
        </form>
    );
}

export default EditUserForm;
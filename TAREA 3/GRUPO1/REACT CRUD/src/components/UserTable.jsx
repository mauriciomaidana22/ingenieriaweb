import React from 'react'


const UserTable = (props) => {

    return ( 
        
        <table>
            <thead>
            <tr >
                <th>ID</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Usuario</th>
                <th>Genero</th>
                
            </tr>
            </thead>
            <tbody>
                {
                    props.users.length > 0 ?
                    props.users.map(user =>(
                    <tr>
                        <td>{user.id}</td>
                        <td>{user.first_name}</td>
                        <td>{user.last_name}</td>
                         <td>{user.username}</td>
                         <td>{user.gender}</td>
                        <td>

                            <button className="button muted-button"
                            onClick ={() => {props.editRow(user)}}>Editar</button>

                            <button className="button muted-button"
                            onClick= {() => {props.deleteUser(user.id)}}>Eliminar</button>
                        </td>
                    </tr>
                    )): (
                    <tr>
                        <td colSpan={3}>No hay usuarios</td>
                    </tr>
                    )
                }
                
                    
            </tbody>
            </table>
        );
        }
 
export default UserTable;
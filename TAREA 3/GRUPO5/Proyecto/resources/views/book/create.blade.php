<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="{{route('book.store')}}" method="post">
    @csrf
        <div>
            <label for="name">name</label>
            <input type="text" name="name" id="name">
        </div>
        <div>
            <label for="author">author</label>
            <input type="text" name="author" id="author">
        </div>
        <div>
            <label for="description">description</label>
            <input type="text" name="description" id="description">
        </div>
        <div>
            <label for="releaseDate">releaseDate</label>
            <input type="date" name="releaseDate" id="releaseDate">
        </div>
        <button>save</button>
    </form>
</body>
</html>
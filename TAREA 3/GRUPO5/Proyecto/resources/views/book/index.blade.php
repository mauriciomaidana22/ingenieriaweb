<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body style="padding:80px">
    <a href="{{route('book.create')}}">new</a>
    <table style="border:solid 1px ; width:80%; margin: auto;">
    
        <thead>
            <td>
                name
            </td>
            <td>
                author
            </td>
            <td>
                description
            </td>
            <td>
                release date
            </td>
        </thead>
        <tbody>
            @foreach($books as $i => $book)
            <tr>
                <td>
                    {{$book->name}}
                </td>
                <td>
                    {{$book->author}}
                </td>
                <td>
                    {{$book->description}}
                </td>
                <td>
                    {{$book->releaseDate}} 
                </td>    
                <td>
                <a href="{{route('book.edit', $book->id)}}">edit</a>
                <a href="{{route('book.show', $book->id)}}">show</a>
                    <form action="{{route('book.destroy',$book->id)}}" method="post">
                    @csrf
                    @method('delete')
                        <button>
                            delete
                        </button>
                    </form>
                </td>        
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
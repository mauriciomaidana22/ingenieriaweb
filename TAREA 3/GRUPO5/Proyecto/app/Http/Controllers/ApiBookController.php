<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\BookRosources;
use App\Http\Resources\BookCollection;
use App\Book;

class ApiBookController extends Controller
{
    public function index()
    {
        return BookCollection::make(Book::all());
    }

    public function update($id, Request $request)
    {
        $book = Book::findOrFail($id);
        $book->update($request->all()); 
        return BookRosources::make($book); 
    }

    public function delete($id)
    {
        $book = Book::findOrFail($id);
        $book->delete(); 
        return BookRosources::make($book); 
    }

    public function store(Request $request)
    {
        $book = Book::create($request->all());
        return BookRosources::make($book); 
    }

    public function show($id)
    {
        $book = Book::findOrFail($id); 
        return BookRosources::make($book); 
    }
}

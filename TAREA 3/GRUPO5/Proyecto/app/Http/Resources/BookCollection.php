<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\BookRosources;

class BookCollection extends ResourceCollection
{
    public $collects = BookRosources::class;
    
    public function toArray($request)
    {
        return [
            'data'=>$this->collection,
            'links'=>[
                'self'=>route('api.book.index')
            ],
            'meta'=>[
                'count'=>$this->collection->count()
            ]
            ];
    }
}

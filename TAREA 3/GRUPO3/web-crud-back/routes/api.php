<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/cars', 'CarController@index')->name('cars.index');

Route::post('/cars', 'CarController@store')->name('cars.store');

Route::get('/cars/{car}', 'CarController@show')->name('cars.show');

Route::put('/cars/{id}', 'CarController@update')->name('cars.update');

Route::delete('/cars/{car}', 'CarController@destroy')->name('cars.destroy');

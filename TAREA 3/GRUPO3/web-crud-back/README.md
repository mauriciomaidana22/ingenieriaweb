# Backend-CRUD

_Backend del CRUD basico para la materia de  Ingenieria Web_

## Comenzando 🚀

_Clonar el contenido de este carpeta para implementa el backend del CRUD._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Herramientas necesarias para instalar el software_

```
Base de datos Postgres v12
PHP >=7.2.0
Editor de codigo (Vs Code)
Composer
Node Package Manager (NPM)
```

### Instalación 🔧

_Abrir la carpeta en su editor de codigo_

_Ejecutar los comandos_

```
composer install
npm install
php artisan key:generate
```
_Crear una base de datos en postgres con el nombre: web-crud
Configurar los siguientes campos del archivo .env_

Ejemplo
```
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=web-crud
DB_USERNAME=postgres
DB_PASSWORD=secret
```
_Ejecutar el comando:_

```
php artisan migrate
```

## Despliegue 📦

_Para levantar el sistema ejecutar_

```
php artisan serve
```
Abre [http://localhost:8000](http://localhost:8000)

## Construido con 🛠️

* [Laravel](https://laravel.com/docs/6.x) - El framework web usado
* [Npm](https://docs.npmjs.com) - Manejador de dependencias
* [Composer](https://getcomposer.org/doc/) - 
Manejador de dependencias
* [Visual Code](https://code.visualstudio.com/docs) - Editor de codigo
* [PostgreSQL](https://www.postgresql.org/docs/) - Gestor de base de datos

## Wiki 📖

Documentacion del proyecto [Wiki](https://drive.google.com/file/d/1_WpTCK1rhHnaxM8ZWlZh-IMkcgk92FHy/view?usp=sharing)

## Versionado 📌

Usamos [GitLab](https://docs.gitlab.com) para el versionado. 

## Autores ✒️

* **Angel Nayib Espinoza Ibañez** - [Nayib](https://gitlab.com/angel98177)
* **Morelia Hidalgo Cáceres** - [Morelia](https://gitlab.com/Morehc)
* **Oscar Ortiz Mayorga** - [Oscar](https://gitlab.com/osortizmayorga)

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.

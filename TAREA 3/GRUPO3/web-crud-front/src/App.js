import React from "react";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "bootstrap/dist/css/bootstrap.css";
import "./App.css";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import EditCar from "./components/edit-car.component";
import CarsList from "./components/cars-listing.component";
import CreateCar from "./components/create-car.component";
import CarsManager from "./components/car-manager.component";

function App() {
  return (<Router>
    <div className="App">
      <header className="header">
        <Navbar bg="secondary" variant="success">
          <Container>

            <Navbar.Brand>
              <Link to={"/cars-manager"} className="nav-link">
              Car manager
              </Link>
            </Navbar.Brand>

            <Nav className="justify-content-end">
              <Nav>
                <Link to={"/create-car"} className="nav-link">
                  Create Car
                </Link>
                <Link to={"/cars-listing"} className="nav-link">
                  Cars List
                </Link>
              </Nav>
            </Nav>

          </Container>
        </Navbar>
      </header>

      <Container>
        <Row>
          <Col md={12}>
            <div className="wrapper">
              <Switch>
                <Route exact path='/' component={CarsManager} />
                <Route path="/create-car" component={CreateCar} />
                <Route path="/edit-car/:id" component={EditCar} />
                <Route path="/cars-listing" component={CarsList} />
                <Route path="/cars-manager" component={CarsManager} />
              </Switch>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  </Router>);
}

export default App;
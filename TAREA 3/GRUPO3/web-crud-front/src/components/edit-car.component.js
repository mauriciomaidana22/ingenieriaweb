import React, { Component } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import axios from 'axios';

export default class EditCar extends Component {

  constructor(props) {
    super(props)

    this.onChangeCarName = this.onChangeCarName.bind(this);
    this.onChangeCarPrice = this.onChangeCarPrice.bind(this);
    this.onChangeCarDescription = this.onChangeCarDescription.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // State
    this.state = {
      name: '',
      price: '',
      description: ''
    }
  }

  componentDidMount() {
    axios.get('http://localhost:8000/api/cars/' + this.props.match.params.id)
      .then(res => {
        this.setState({
          name: res.data.name,
          price: res.data.price,
          description: res.data.description
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }

  onChangeCarName(e) {
    this.setState({ name: e.target.value })
  }

  onChangeCarPrice(e) {
    this.setState({ price: e.target.value })
  }

  onChangeCarDescription(e) {
    this.setState({ description: e.target.value })
  }

  onSubmit(e) {
    e.preventDefault()

    const carObject = {
      name: this.state.name,
      price: this.state.price,
      description: this.state.description
    };

    axios.put('http://localhost:8000/api/cars/' + this.props.match.params.id, carObject)
      .then((res) => {
        console.log(res.data)
        console.log('Car successfully updated')
      }).catch((error) => {
        console.log(error)
      })

    // Redirect to Car List 
    this.props.history.push('/cars-listing')
  }


  render() {
    return (<div className="form-wrapper">
      <Form onSubmit={this.onSubmit}>
        <Form.Group controlId="Name">
          <Form.Label>Nombre</Form.Label>
          <Form.Control type="text" value={this.state.name} onChange={this.onChangeCarName} />
        </Form.Group>

        <Form.Group controlId="Price">
          <Form.Label>Precio</Form.Label>
          <Form.Control type="number" value={this.state.price} onChange={this.onChangeCarPrice} />
        </Form.Group>

        <Form.Group controlId="Description">
          <Form.Label>Descripcion</Form.Label>
          <Form.Control type="text" value={this.state.description} onChange={this.onChangeCarDescription} />
        </Form.Group>

        <Button variant="danger" size="lg" block="block" type="submit">
          Actualizar auto
        </Button>
      </Form>
    </div>);
  }
}
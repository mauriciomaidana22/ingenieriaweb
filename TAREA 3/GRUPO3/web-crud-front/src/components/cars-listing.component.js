import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';
import CarTableRow from './CarTableRow';


export default class CarList extends Component {

  constructor(props) {
    
    super(props)
    this.state = {
      cars: []
    };
  }
  getAutos() {
    axios.get('http://localhost:8000/api/cars/')
    // .then((res) => console.log(res.data))  
    .then((res) => {
      this.setState({
        cars: res.data
      });
    })
    .catch((error) => {
      
      console.log(error);
    })
  }

  componentDidMount() {
    this.getAutos();    
  }
 
  DataTable() {
    return this.state.cars.map((res, i) => {
      return <CarTableRow obj={res} key={i} />;
    });
  }


  render() {
    return (<div className="table-wrapper">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Nomnbre</th>
            <th>Precio</th>
            <th>Descripcion</th>
            <th>Accion</th>
          </tr>
        </thead>
        <tbody>
          {this.DataTable()}
        </tbody>
      </Table>
    </div>);
  }
}
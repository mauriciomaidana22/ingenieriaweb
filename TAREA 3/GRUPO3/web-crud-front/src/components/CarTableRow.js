import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import Swal from 'sweetalert2';


export default class CarTableRow extends Component {
    constructor(props) {
        
        super(props);
        this.deleteCar = this.deleteCar.bind(this);
    }
    
    deleteCar() {
            Swal.fire({
                title: 'Estas seguro?',
                text: "No podrás revertir esto!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, bórralo!',
                cancelButtonText: 'Cancelar',
              }).then((result) => {
                if (result.isConfirmed) {
                    axios.delete('http://localhost:8000/api/cars/' + this.props.obj.id)
                    .then((res) => {
                        console.log('Car removed deleted!')
                    }).catch((error) => {
                        console.log(error)
                    })
                    Swal.fire(
                    'Eliminado!',
                    'El registro ha sido eliminado.',
                    'success'
                  )
                    //this.props.children.push("/cars-listing");
                    
                }
              })    
            
    }
    render() {
        return (
            <tr>
                <td>{this.props.obj.name}</td>
                <td>{this.props.obj.price}</td>
                <td>{this.props.obj.description}</td>
                <td>
                    <Link className="edit-link" to={"/edit-car/" + this.props.obj.id}>
                       <Button size="sm" variant="info">Editar</Button>
                    </Link>
                    
                    <Button onClick={this.deleteCar} size="sm" variant="danger">Eliminar</Button>
                </td>
            </tr>
        );
    }
}
# Frontend-CRUD

_Frontend del CRUD basico para la materia de  Ingenieria Web_

## Comenzando 🚀

_Clonar el contenido de este carpeta para implementa el frontend del CRUD._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Herramientas necesarias para instalar el software_

```
Editor de codigo (Vs Code)
Node Package Manager (NPM)
API de Laravel (Backend)
```

### Instalación 🔧

_Abrir la carpeta en su editor de codigo_

_Ejecutar el comando_

```
npm install package.json
```

## Despliegue 📦

_Para levantar el sistema ejecutar_

```
npm start
```
Abre [http://localhost:3000](http://localhost:3000)

## Construido con 🛠️

* [Reactjs](https://reactjs.org/docs/getting-started.html) - El framework web usado
* [Npm](https://docs.npmjs.com) - Manejador de dependencias
* [Visual Code](https://code.visualstudio.com/docs) - Editor de codigo


## Wiki 📖

Documentacion del proyecto [Wiki](https://drive.google.com/file/d/1_WpTCK1rhHnaxM8ZWlZh-IMkcgk92FHy/view?usp=sharing)

## Versionado 📌

Usamos [GitLab](https://docs.gitlab.com) para el versionado. 

## Autores ✒️

* **Angel Nayib Espinoza Ibañez** - [Nayib](https://gitlab.com/angel98177)
* **Morelia Hidalgo Cáceres** - [Morelia](https://gitlab.com/Morehc)
* **Oscar Ortiz Mayorga** - [Oscar](https://gitlab.com/osortizmayorga)

## License

Reactjs is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.

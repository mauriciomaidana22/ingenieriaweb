<?php

namespace App\Http\Controllers;

use App\Personal;
use Illuminate\Http\Request;

class PersonalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personal = Personal::all();
        return response()->json($personal);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Name' => 'required',
            'LastName' => 'required',
            'Cell' => 'required', 
            'Email' => 'required', 
            'Age' => 'required'
        ]);
        $personal = Personal::create($request->all());
        return response()->json(['message'=> 'personal created', 
        'personal' => $personal]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Personal  $personal
     * @return \Illuminate\Http\Response
     */
    public function show(Personal $personal)
    {
        return $personal;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Personal  $personal
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Personal $personal)
    {
        //
        $request->validate([
            'Name' => 'required',
            'LastName' => 'required',
            'Cell' => 'required', 
            'Email' => 'required', 
            'Age' => 'required'
        ]);
        $personal->Name = $request->Name();
        $personal->LastName = $request->LastName();
        $personal->Cell = $request->Cell();
        $personal->Email = $request->Email();
        $personal->Age = $request->Age();
        $personal->save();
        //$personal=Personal::findOrFail($id);
        //$personal->update($request->all());
        
        return response()->json([
            'message' => 'personal updated!',
            'personal' => $personal
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Personal  $personal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $personal = Personal::where('id', '=', $id)->first();

        $personal->update($request->all());
        return response()->json([
            'message' => 'personal updated!',
            'personal' => $personal
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Personal  $personal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Personal $personal)
    {
        $personal->delete();
        return response()->json([
            'message' => 'personal deleted'
        ]);
    }
}

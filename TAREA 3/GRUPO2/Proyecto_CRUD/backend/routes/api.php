<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//POSIBLE MODIFICACION 

Route::get('/personal', 'PersonalController@index')->name('personal.index');

Route::post('/personal', 'PersonalController@store')->name('personal.store');

Route::get('/personal/{personal}', 'PersonalController@show')->name('personal.show');

Route::put('/personal/{personal}', 'PersonalController@update')->name('personal.update');

Route::delete('/personal/{personal}', 'PersonalController@destroy')->name('personal.destroy');
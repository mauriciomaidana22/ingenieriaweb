@extends('layout')
@section('content')
                    <form method="POST" action="{{ route('producto.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Nombre</label>
                            <input name="nombre" type="text" class="form-control" id="exampleFormControlInput1" placeholder="Nombre...">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Precio</label>
                            <input name="precio" type="number" step="0.1" class="form-control" id="exampleFormControlInput1" placeholder="Nombre...">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Descripcion</label>
                            <textarea name="descripcion" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Descripcion..."></textarea>
                        </div>
                        <button class="btn btn-primary" type="success">Guardar</button>
                    </form>
                    @endsection

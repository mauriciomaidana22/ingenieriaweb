@extends('layout')
@section('content')
    
                    <a class="btn btn-success mb-5" href="{{ route('producto.create') }}">Nuevo</a>
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Precio</th>
                                <th scope="col">Descripcion</th>
                                <th scope="col">opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($productos as $i => $producto)
                                <tr>
                                    <th scope="row">{{ $i + 1 }}</th>
                                    <td>{{ $producto->nombre }}</td>
                                    <td>{{ $producto->precio }}</td>
                                    <td>{{ $producto->descripcion }}</td>
                                    <td>
                                        <a class="btn btn-warning" href="{{ route('producto.edit', $producto->id) }}">Editar</a>
                                        <form action="{{ route('producto.destroy', $producto->id) }}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger">Eliminar</button>
                                        </form>
                                        <a class="btn btn-primary" href="{{ route('producto.show', $producto->id) }}">ver</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endsection

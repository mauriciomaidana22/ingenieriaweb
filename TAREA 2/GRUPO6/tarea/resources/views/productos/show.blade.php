@extends('layout')

@section('content')
<form method="POST" action="{{ route('producto.update', $producto->id) }}">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="exampleFormControlInput1">Nombre</label>
        <input readonly value="{{ $producto->nombre }}"  name="nombre" type="text" class="form-control" id="exampleFormControlInput1" placeholder="Nombre...">
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Precio</label>
        <input readonly value="{{ $producto->precio }}" name="precio" type="number" step="0.1" class="form-control" id="exampleFormControlInput1" placeholder="Nombre...">
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Descripcion</label>
        <textarea readonly name="descripcion" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Descripcion..."> {{ $producto->descripcion }}</textarea>
    </div>
</form>
@endsection
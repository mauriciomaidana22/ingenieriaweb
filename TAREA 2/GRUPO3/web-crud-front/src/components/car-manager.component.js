import React, { Component } from "react";
import CarsList from './cars-listing.component';
import CreateCar from "./create-car.component";

export default class CarManager extends Component {


    render() {
        return(
            <div>
                <CreateCar />
                <CarsList />
            </div>
        )
    }
}
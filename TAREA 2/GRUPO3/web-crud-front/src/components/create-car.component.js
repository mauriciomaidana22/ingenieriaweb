import React, { Component } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import axios from 'axios'
import Swal from 'sweetalert2';


export default class CreateCar extends Component {
      constructor(props) {
    super(props)

    // Setting up functions
    this.onChangeCarName = this.onChangeCarName.bind(this);
    this.onChangeCarPrice = this.onChangeCarPrice.bind(this);
    this.onChangeCarDescription = this.onChangeCarDescription.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // Setting up state
    this.state = {
      name: '',
      description: '',
      price: ''
    }
  }

  onChangeCarName(e) {
    this.setState({name: e.target.value})
  }

  onChangeCarPrice(e) {
    this.setState({price: e.target.value})
  }

  onChangeCarDescription(e) {
    this.setState({description: e.target.value})
  }

  onSubmit(e) {
    e.preventDefault()
     const car = {
      name: this.state.name,
      price: this.state.price,
      description: this.state.description
    };
    axios.post('http://localhost:8000/api/cars/', car)
      .then(res => console.log(res.data));
    // console.log(`Car successfully created!`);
    // console.log(`Name: ${this.state.name}`);
    // console.log(`Price: ${this.state.price}`);
    // console.log(`Description: ${this.state.description}`);
    Swal.fire(
  'Buen trabajo!',
  'Auto añadido correctamente',
  'success'
)

    this.setState({name: '', price: '', description: ''})
  }

  render() {
    return (<div className="form-wrapper">
      <Form onSubmit={this.onSubmit}>
        <Row> 
            <Col>
             <Form.Group controlId="Name">
                <Form.Label>Nombre</Form.Label>
                <Form.Control type="text" pattern="[A-Za-z]+" required title="Solo letras mayusculas y minusculas" value={this.state.name} onChange={this.onChangeCarName}/>
             </Form.Group>
            
            </Col>
            
            <Col>
             <Form.Group controlId="Price">
                <Form.Label>Precio</Form.Label>
                        <Form.Control type="number" pattern="[0-9]+" required title="Solo numeros" min="0" value={this.state.price} onChange={this.onChangeCarPrice}/>
             </Form.Group>
            </Col>  
           
        </Row>
            

        <Form.Group controlId="description">
          <Form.Label>Descripcion</Form.Label>
                <Form.Control as="textarea" type="textarea" value={this.state.description} onChange={this.onChangeCarDescription}/>
        </Form.Group>

       
        <Button variant="primary" size="lg" block="block" type="submit">
          Añadir auto
        </Button>
      </Form>
      <br></br>
      <br></br>

      
    </div>);
  }
}


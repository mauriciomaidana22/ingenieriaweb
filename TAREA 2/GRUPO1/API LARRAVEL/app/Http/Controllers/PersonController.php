<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PersonModel;
use Illuminate\Contracts\Validation\Validator;
class PersonController extends Controller
{

    function getAll()
    {
        $data = PersonModel::all();

        if($data) {
            return [
                "data"          =>$data,
                "statusCode"    => 200,
                "status"        => "ok",
                "message"       => "Correct Data Extraction"
            ];
        } else {
            return [
                "statusCode"    => 500,
                "message"       => "Internal Server Error"
            ];
        }
    }

    function create(Request $request)
    {
        $person = new PersonModel;

        $person->first_name     = $request->first_name;
        $person->last_name      = $request->last_name;
        $person->username       = $request->username;
        $person->password       = $request->password;
        $person->gender         = $request->gender;

        $result = $person->save();

        if ($result) {
            return [
                "data"          =>  $person,
                "statusCode"    =>  201,
                "status"        =>  "ok",
                "message"       =>"Correct Insertion"
            ];
        } else {
            return [
                "statusCode"    => 500,
                "message"       => "Internal Server Error"
            ];
        }
    }

    function update(Request $request)
    {
        $person = PersonModel::find($request->id);

        $person->first_name     = $request->first_name;
        $person->last_name      = $request->last_name;
        $person->username       = $request->username;
        $person->password       = $request->password;
        $person->gender         = $request->gender;

        $result = $person->save();

        if ($result) {
            return [
                "data"          =>  $person,
                "statusCode"    =>  200,
                "status"        =>  "ok",
                "message"       =>"Correct Update"
            ];
        } else {
            return [
                "statusCode"    => 500,
                "message"       => "Internal Server Error"
            ];
        }
    }

    function delete($id)
    {
        $person = PersonModel::find($id);
        $result = $person->delete();

        if ($result) {
            return [
                "statusCode"    =>  200,
                "status"        =>  "ok",
                "message"       =>"Correct Delete"
            ];
        } else {
            return [
                "statusCode"    => 500,
                "message"       => "Internal Server Error"
            ];
        }
    }

    function getById($id)
    {
        $result = PersonModel::where("id",$id)->get();
        //$result = PersonModel::where("name","like","%".$name."%")->get();

        if($result) {
            return [
                "data"          =>$result,
                "statusCode"    => 200,
                "status"        => "ok",
                "message"       => "Correct Data Extraction"
            ];
        } else {
            return [
                "statusCode"    => 500,
                "message"       => "Internal Server Error"
            ];
        }
    }

    /*function save(Request $request)
    {
        $rules = array(
            "first_name"=>"required|min:2|max:4"
        );

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails) {
            //return $validator->error();
            return response()->json($validator->errors(),401);
        } else {
            return ["data"=>"correct"];
        }
    }*/
}

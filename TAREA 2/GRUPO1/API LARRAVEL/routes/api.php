<?php

use App\Http\Controllers\PersonController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('person', [PersonController::class,'getAll']);
Route::post('person', [PersonController::class, 'create']);
Route::put('person', [PersonController::class, 'update']);
Route::delete('person/{id}', [PersonController::class,'delete']);
Route::get('person/{id}', [PersonController::class, 'getById']);

Route::post('save/validation', [PersonController::class, 'save']);

Route::resource('test', PersonController::class);

/*Route::get('/demo-url',  function  (Request $request)  {
    return response()->json(['Laravel 7 CORS Demo']);
 });*/
